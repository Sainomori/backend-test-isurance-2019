<?php


namespace Isurance\Algorithms;


class SpecialSum
{
    /**
     * @deprecated
     * @var array
     */
    private array $debugHash = [];
    private array $calculatedFactorials = [];

    /**
     * @param $k
     * @param $n
     * @return string
     * @throws SpecialSumException
     */
    public function getSpecialSum(int $k, int $n): string
    {
        $coefficientArray = $this->getCoefficientsArray($k, $n);
        $sum = 0;
        for ($i = $n; $i > 0; $i--) {
            $sum += $i * $coefficientArray[$n - $i];
        }
        return strval($sum);
    }

    /**
     * @param $k
     * @param $n
     * @return array
     * @throws SpecialSumException
     */
    protected function getCoefficientsArray(int $k, int $n): array
    {
        $return = [];
        for ($i = 0; $i <= $n; $i++) {
            $return[] = $this->fact($k - 1) / ($this->fact($k - 1 - $i) * $this->fact($i));
            $k++;
        }
        return ($return);
    }

    /**
     * @param int $i
     * @return float
     * @throws SpecialSumException
     */
    protected function fact(int $i): float
    {
        if (!isset($this->calculatedFactorials[$i])) {
            $factorial = 1;
            for ($k = 1; $k <= $i; $k++) {
                $factorial = $factorial * $k;
                if ($factorial >= PHP_FLOAT_MAX) {
                    throw new SpecialSumException('Too big factorial');
                }
            }
            $this->calculatedFactorials[$i] = $factorial;
        }
        return $this->calculatedFactorials[$i];
    }

    /**
     * @param int $k
     * @param int $n
     * @return int
     * @deprecated
     */
    public function specialSumWithoutBrain(int $k, int $n): int
    {
        if ($k === 0) {
            if (isset($this->debugHash["$k.$n"])) {
                $this->debugHash["$k.$n"]++;
            } else {
                $this->debugHash["$k.$n"] = 1;
            }
        }

        if ($k === 0) {
            return $n;
        }
        $return = 0;
        for ($i = 1; $i <= $n; $i++) {
            $return += $this->specialSumWithoutBrain($k - 1, $i);
        }
        return $return;
    }

    /**
     * @return array
     * @deprecated
     */
    public function getDebugHash(): array
    {
        return $this->debugHash;
    }
}