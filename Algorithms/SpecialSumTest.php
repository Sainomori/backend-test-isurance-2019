<?php
declare(strict_types=1);

namespace Isurance\Algorithms;

use PHPUnit\Framework\TestCase;

class SpecialSumTest extends TestCase
{
    /**
     * @var SpecialSum
     */
    private SpecialSum $ss;

    /**
     * @param $k
     * @param $n
     * @param $expected
     * @dataProvider dataProvider
     */
    public function testGetSpecialSum($k, $n, $expected)
    {
        $this->assertSame($expected, $this->ss->getSpecialSum($k, $n));
    }

    public function testGetSpecialSumException()
    {
        $this->expectException(SpecialSumException::class);
        $this->ss->getSpecialSum(100, 100);
    }

    public function dataProvider(): array
    {
        return [
            "1-3" => [1, 3, '6'],
            "2-3" => [2, 3, '10'],
            "4-10" => [4, 10, '2002'],
            "10-10" => [10, 10, '167960'],
            "20-20" => [20, 20, '131282408400'],
            "30-30" => [30, 30, '1.1444959506277E+17'],
        ];
    }

    protected function setUp(): void
    {
        $this->ss = new SpecialSum();
    }

}
