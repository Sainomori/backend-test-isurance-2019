<?php


namespace Isurance\Algorithms;


use Exception;

class SpecialSumException extends Exception
{
    public function __toString(): string
    {
        return 'Sorry, but I can\'t calculate such a big number =(';
    }
}