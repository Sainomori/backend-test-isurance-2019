<?php
declare(strict_types=1);

namespace Isurance\Algorithms;

use PHPUnit\Framework\TestCase;

class SpecialSumSpaceInefficientTest extends TestCase
{
    /**
     * @var SpecialSumSpaceInefficient
     */
    private SpecialSumSpaceInefficient $ss;

    /**
     * @param $k
     * @param $n
     * @param $expected
     * @dataProvider dataProvider
     */
    public function testGetSpecialSum($k, $n, $expected)
    {
        $this->assertSame($expected, $this->ss->getSpecialSum($k, $n));
    }

    public function dataProvider(): array
    {
        return [
            "1-3" => [1, 3, '6'],
            "2-3" => [2, 3, '10'],
            "4-10" => [4, 10, '2002'],
            "10-10" => [10, 10, '167960'],
            "20-20" => [20, 20, '131282408400'],
            "30-30" => [30, 30, '114449595062769120'],
            "100-100" => [100, 100, '8.9651994709013E+58'],
        ];
    }

    protected function setUp(): void
    {
        $this->ss = new SpecialSumSpaceInefficient();
    }

}
