<?php


namespace Isurance\Algorithms;


class SpecialSumSpaceInefficient extends SpecialSum
{
    /**
     * @var int[][]
     */
    private array $triangle;

    protected function getCoefficientsArray(int $k, int $n): array
    {
        $maxRow = $k + $n - 1;
        $this->buildTriangle($maxRow);
        $coefficientsArray = [];
        $position = 0;
        for ($row = $k - 1; $row <= $maxRow; $row++) {
            $coefficientsArray[] = $this->triangle[$row][$position];
            $position++;
        }
        return $coefficientsArray;
    }

    private function buildTriangle(int $maxRow)
    {
        $this->triangle = array(
            array(1),
        );

        for ($i = 1; $i <= $maxRow; ++$i) {
            $prevCount = count($this->triangle[$i - 1]);
            for ($j = 0; $j <= $prevCount; ++$j) {
                $this->triangle[$i][$j] = (
                    (isset($this->triangle[$i - 1][$j - 1]) ? $this->triangle[$i - 1][$j - 1] : 0) +
                    (isset($this->triangle[$i - 1][$j]) ? $this->triangle[$i - 1][$j] : 0)
                );
            }
        }
    }
}