<?php
require_once __DIR__ . '/../bootstrap.php';

use Isurance\OOProgramming\Logger\ErrorLogger;
use Isurance\OOProgramming\Logger\InfoLogger;
use Isurance\OOProgramming\Logger\Logger;
use Isurance\OOProgramming\Parser\FeedParser;
use Isurance\OOProgramming\Strategies\Feed\ProductFeed;
use Isurance\OOProgramming\Strategies\Format\CsvFormat;
use Isurance\OOProgramming\Strategies\Format\XmlFormat;
use Isurance\OOProgramming\Strategies\Transport\FileTransport;
use Isurance\OOProgramming\Strategies\Transport\UrlTransport;
use Psr\Log\LogLevel;

/**
 * usually this should be solved by DI container and CompilerPass
 */
$loggers = [
    LogLevel::ERROR => new ErrorLogger(),
    LogLevel::INFO => new InfoLogger()
];
$psrLogger = new Logger();

$transportStrategies = [
    new UrlTransport($psrLogger),
    new FileTransport($psrLogger)
];
$formatStrategies = [
    new CsvFormat($psrLogger),
    new XmlFormat($psrLogger)
];
$feedStrategies = [
    new ProductFeed($psrLogger)
];

$parser = new FeedParser(
    'products.xml',
    $transportStrategies,
    $formatStrategies,
    $feedStrategies
);
foreach ($loggers as $level => $logger) {
    $parser->addObserver($logger, $level);
}

echo $parser->parse() . PHP_EOL;




