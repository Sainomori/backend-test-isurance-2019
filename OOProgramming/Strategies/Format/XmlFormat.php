<?php


namespace Isurance\OOProgramming\Strategies\Format;


use Exception;
use Isurance\OOProgramming\Strategies\AbstractStrategy;

class XmlFormat extends AbstractStrategy implements FormatStrategyInterface
{

    public function canHandle(string $feedUri): bool
    {
        if (pathinfo($feedUri)['extension'] === 'xml') {
            $this->info('XmlFormat will handle this file');
            return true;
        }
        $this->info('XmlFormat can\'t handle this file');
        return false;
    }

    public function handle($content): array
    {
        $this->info('XmlFormat starts raw parsing');
        if (empty($content)) {
            $this->error('XmlFormat founds empty file');
            throw new Exception('Empty file');
        }

        $result = [];
        $documentObject = simplexml_load_string($content);
        foreach ($documentObject->item as $item) {
            $result[] = (array)$item;
        }
        $this->info('XmlFormat ends raw parsing');
        return $result;
    }
}