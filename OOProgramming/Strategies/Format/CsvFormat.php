<?php


namespace Isurance\OOProgramming\Strategies\Format;


use Isurance\OOProgramming\Strategies\AbstractStrategy;

class CsvFormat extends AbstractStrategy implements FormatStrategyInterface
{

    public function canHandle(string $feedUri): bool
    {
        if (pathinfo($feedUri)['extension'] === 'csv') {
            $this->info('CsvFormat will handle this file');
            return true;
        }
        $this->info('CsvFormat can\'t handle this file');
        return false;
    }

    public function handle($content): array
    {
        // TODO: Implement handle() method.
    }
}