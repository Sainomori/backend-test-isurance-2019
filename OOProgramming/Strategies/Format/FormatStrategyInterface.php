<?php


namespace Isurance\OOProgramming\Strategies\Format;


use Isurance\OOProgramming\Strategies\StrategyInterface;

interface FormatStrategyInterface extends StrategyInterface
{
    public function handle(string $content): array;
}