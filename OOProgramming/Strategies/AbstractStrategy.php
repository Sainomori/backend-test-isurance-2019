<?php


namespace Isurance\OOProgramming\Strategies;


use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;

abstract class AbstractStrategy implements StrategyInterface, LoggerAwareInterface
{
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    public function __construct(?LoggerInterface $logger)
    {
        $this->setLogger($logger);
    }

    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function info($message, array $context = array()) {
        $this->logger->info($message, $context);
    }

    public function error($message, $context = array()) {
        $this->logger->error($message, $context);
    }
}