<?php


namespace Isurance\OOProgramming\Strategies;


use Psr\Log\LoggerInterface;

interface StrategyInterface
{
    public function __construct(LoggerInterface $logger);
    public function canHandle(string $feedUri): bool;
}