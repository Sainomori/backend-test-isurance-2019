<?php


namespace Isurance\OOProgramming\Strategies\Feed;


use Isurance\OOProgramming\Entity\Product;
use Isurance\OOProgramming\Strategies\StrategyInterface;

interface FeedStrategyInterface extends StrategyInterface
{
    /**
     * @param array $entities
     * @return Product[]
     */
    public function handle(array $entities): array;
}