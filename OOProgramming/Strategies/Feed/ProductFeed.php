<?php


namespace Isurance\OOProgramming\Strategies\Feed;


use DateTime;
use Exception;
use Isurance\OOProgramming\Entity\Product;
use Isurance\OOProgramming\Strategies\AbstractStrategy;

class ProductFeed extends AbstractStrategy implements FeedStrategyInterface
{

    public function canHandle(string $feedUri): bool
    {
        if (pathinfo($feedUri)['filename'] === 'products') {
            $this->info('ProductFeed will handle this feed');
            return true;
        }
        return false;
    }

    /**
     * @param array $rawEntities
     * @return Product[]
     * @throws Exception
     */
    public function handle(array $rawEntities): array
    {
        $this->info('ProductFeed starts parsing this feed');
        $result = [];
        foreach ($rawEntities as $key => $entity) {
            $product = new Product(
                $key,
                $entity['title'] ?? null,
                $entity['link'] ?? null,
                new DateTime($entity['pubDate'] ?? null)
            );
            if ($product->isValid()) {
                $result[] = $product;
                $this->info("Entity {$product->getTitle()} added");
            } else {
                $this->error("Entity is not valid");
            }
        }
        $this->info('ProductFeed ends parsing this feed');
        return $result;
    }
}