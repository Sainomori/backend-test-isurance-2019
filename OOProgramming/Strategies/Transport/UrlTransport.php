<?php


namespace Isurance\OOProgramming\Strategies\Transport;


use Isurance\OOProgramming\Strategies\AbstractStrategy;

class UrlTransport extends AbstractStrategy implements TransportStrategyInterface
{

    public function canHandle(string $feedUri): bool
    {
        if (filter_var($feedUri, FILTER_VALIDATE_URL) === FALSE) {
            $this->info('UrlTransport can\'t handle this url');
            return false;
        }
        $this->info('UrlTransport will handle this url');
        return true;
    }

    public function handle($feedUri): string
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $feedUri);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
}