<?php


namespace Isurance\OOProgramming\Strategies\Transport;


use Exception;
use Isurance\OOProgramming\Strategies\AbstractStrategy;

class FileTransport extends AbstractStrategy implements TransportStrategyInterface
{

    public function canHandle(string $feedUri): bool
    {
        if (filter_var($feedUri, FILTER_VALIDATE_URL) === FALSE) {
            $this->info('FileTransport will handle this path');
            return true;
        }
        $this->info('FileTransport can\'t handle this path');
        return false;
    }

    public function handle($feedUri): string
    {

        $this->info('FileTransport starts reading file');
        if (file_exists($feedUri)) {
            $return = file_get_contents($feedUri);
        } else {
            $this->error('FileTransport can\'t find the file!');
            throw new Exception('File not found');
        }
        $this->info('FileTransport ends reading file');
        return $return;
    }
}