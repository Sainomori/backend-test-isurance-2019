<?php


namespace Isurance\OOProgramming\Strategies\Transport;


use Isurance\OOProgramming\Strategies\StrategyInterface;

interface TransportStrategyInterface extends StrategyInterface
{
    public function handle(string $feedUri): string;
}