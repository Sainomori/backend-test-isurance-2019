<?php


namespace Isurance\OOProgramming\Parser;


use Exception;
use Isurance\OOProgramming\Observer\IObservable;
use Isurance\OOProgramming\Strategies\Feed\FeedStrategyInterface;
use Isurance\OOProgramming\Strategies\Format\FormatStrategyInterface;
use Isurance\OOProgramming\Strategies\Transport\TransportStrategyInterface;
use Psr\Log\LogLevel;

class FeedParser extends FeedParserBase implements IObservable
{
    /**
     * FeedParser constructor.
     * @param $strFeedUrl
     * @param TransportStrategyInterface[] $transportStrategies
     * @param FormatStrategyInterface[] $formatStrategies
     * @param FeedStrategyInterface[] $feedStrategies
     */
    public function __construct(
        $strFeedUrl,
        private array $transportStrategies,
        private array $formatStrategies,
        private array $feedStrategies,
    )
    {
        parent::__construct($strFeedUrl);
    }

    function parse()
    {
        $content = null;
        $parsedContent = null;
        $transport = null;
        $format = null;
        $feed = null;

        foreach ($this->transportStrategies as $transportStrategy) {
            if ($transportStrategy->canHandle($this->getFeedUrl())) {
                $transport = $transportStrategy;
                break;
            }
        }
        if (is_null($transport)) {
            $this->fireEvent(LogLevel::ERROR, 'No suitable transport found');
            exit;
        }
        foreach ($this->formatStrategies as $formatStrategy) {
            if ($formatStrategy->canHandle($this->getFeedUrl())) {
                $format = $formatStrategy;
                break;
            }
        }
        if (is_null($format)) {
            $this->fireEvent(LogLevel::ERROR, 'No suitable format found');
            exit;
        }
        foreach ($this->feedStrategies as $feedStrategy) {
            if ($feedStrategy->canHandle($this->getFeedUrl())) {
                $feed = $feedStrategy;
                break;
            }
        }
        if (is_null($feed)) {
            $this->fireEvent(LogLevel::ERROR, 'No suitable feed parser found');
            exit;
        }

        try {
            $result = $feed->handle($format->handle($transport->handle($this->getFeedUrl())));
            foreach ($result as $entity) {
                echo $entity . "\n\n";
            }
        } catch (Exception $e) {
            $this->fireEvent(LogLevel::ERROR, $e->getMessage());
            exit;
        }
    }
}