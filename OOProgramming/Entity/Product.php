<?php


namespace Isurance\OOProgramming\Entity;


use DateTime;

class Product
{
    public function __construct(
        private ?int $id,
        private ?string $title,
        private ?string $link,
        private ?DateTime $pubDate
    )
    {
    }

    public function isValid(): bool
    {
        return !empty($this->title) && !empty($this->link);
    }

    public function __toString(): string
    {
        return <<<EOD
Name: $this->title | Id: $this->id | Date: {$this->getPubDateString()}
$this->link
EOD;
    }

    public function getPubDateString(): string
    {
        return $this->pubDate->format('Y-m-d H:i:s');
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Product
     */
    public function setId(?int $id): Product
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     * @return Product
     */
    public function setTitle(?string $title): Product
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @param string|null $link
     * @return Product
     */
    public function setLink(?string $link): Product
    {
        $this->link = $link;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getPubDate(): ?DateTime
    {
        return $this->pubDate;
    }

    /**
     * @param DateTime|null $pubDate
     * @return Product
     */
    public function setPubDate(?DateTime $pubDate): Product
    {
        $this->pubDate = $pubDate;
        return $this;
    }

    /**
     * @param string $pubDate
     * @return Product
     */
    public function setPubDateFromString(string $pubDate): Product
    {
        return $this;
    }
}
